# pre-commit security ref

# How configure

- pip install pre-commit or pip install -r requirements.txt
- pre-commit --version
- create a file named .pre-commit-config.yaml
- pre-commit install
- pre-commit run --all-files